from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from telegram import ReplyKeyboardMarkup, ParseMode, Bot, emoji, ReplyKeyboardHide, ForceReply
from telegram.emoji import Emoji
import pickledb
import logging
import unibotpy
from tabulate import tabulate
from it.unibot.unibotpy import orario_lab, lab_free
from telegram.ext.stringcommandhandler import StringCommandHandler
from telegram.ext.stringregexhandler import StringRegexHandler
from telegram.ext.regexhandler import RegexHandler
import re
from telegram.ext.inlinequeryhandler import InlineQueryHandler
from uuid import uuid4
from telegram.inlinequeryresult import InlineQueryResult
from telegram.inputtextmessagecontent import InputTextMessageContent
from telegram.inlinequeryresultarticle import InlineQueryResultArticle
db = pickledb.load('db/allstate.states', False)
token = 'QUI VA INSERITO IL TOKEN CREATO DA BOT-FATHER'
updater = Updater(token)  # token bot
dp = updater.dispatcher

def start(bot, update):
    text = "<b>Benvenuti nel bot</b>\n" + u'\u2328' + "Lista dei comandi: " + u'\u2328' + "\n" + u'\U0001f3db' + " /aule: Lista delle aule\n"\
            + u'\U0001F52C' + " /lab: Lista dei laboratori" + "\n" + u'\U0001f193' + u'\U0001F52C' + " /labfree: Laboratori liberi" + "\n" \
            +  u'\U0001f50d' + " /cerca: Ricerca di un insegnamento o un docente\n" \
            + u'\u2754' + "/info: Informazioni sul bot"
    bot.sendMessage(chat_id=update.message.chat_id, text=text, parse_mode=ParseMode.HTML, reply_markup=ReplyKeyboardHide(hide_keyboard=True))


#===============================================================================
# Invia un messaggio con la lista dei laboratori
#===============================================================================
def lab(bot, update):
    lab = unibotpy.lab()
    bot.sendMessage(chat_id=update.message.chat_id, text=lab, parse_mode=ParseMode.HTML, reply_markup=ReplyKeyboardHide(hide_keyboard=True))
    
    
def prenotazione_lab(bot, update):
    pren = update.message.text[1:]  # elimino /
    dettaglio = unibotpy.dett_prenotazione_lab(pren)
    bot.sendMessage(chat_id=update.message.chat_id, text=dettaglio, parse_mode=ParseMode.HTML)


def lab_free(bot,update):
    sel = unibotpy.SELEZIONA_GIORNO
    key = unibotpy.labfree_day_markup()
    bot.sendMessage(chat_id=update.message.chat_id, text=sel, reply_markup=ReplyKeyboardMarkup(key, resize_keyboard=True, one_time_keyboard=True))
    

#===============================================================================
# Analizza tutti gli input in arrivo (no comandi)
#=============================================================================== 
def input_parser(bot, update):
    mex = update.message.text 
    id_user = update.message.from_user["id"]  # id utente
    if (mex.find(u"\U0001F52C") == 0):  # lab button #microscopee #formato (Microscope)[int(lab)] {LUN|MAR|MER|GIO|VEN} len= 7
        grid_day, prenList = orario_lab(mex[2], mex[5:])  # 1 -> numero lab 2-> giorno settimana
        for prenotazione in prenList:
            dp.add_handler(CommandHandler(prenotazione, prenotazione_lab))  # crea gli handler per ogni prenotazione restituita
        bot.sendMessage(chat_id=update.message.chat_id, text=grid_day, parse_mode=ParseMode.HTML)
    if (mex.find(u'\U0001f193') == 0): #free button Laboratori liberi
        day = mex[3:]
        grid_lab1, grid_lab2, grid_lab3 = unibotpy.lab_free(day)
        bot.sendMessage(chat_id=update.message.chat_id, text="<pre>" + grid_lab1 + "</pre>", parse_mode=ParseMode.HTML)
        bot.sendMessage(chat_id=update.message.chat_id, text="<pre>" + grid_lab2 + "</pre>", parse_mode=ParseMode.HTML)
        bot.sendMessage(chat_id=update.message.chat_id, text="<pre>" + grid_lab3 + "</pre>", parse_mode=ParseMode.HTML)
    else:  # se l'utente era in uno stato di attesa per la ricerca e ha inserito il testo di ricerca, viene restituito il risultato
        state = db.get(id_user)
        if (state == 'need_search'):
            inf, keyList = unibotpy.search(mex, False)
            for key in keyList:
                    dp.add_handler(CommandHandler(key, orario_materia))  # crea gli handler per ogni materia restituita
            bot.sendMessage(chat_id=update.message.chat_id, text=inf, parse_mode=ParseMode.HTML, reply_markup=ReplyKeyboardHide(hide_keyboard=True))
            db.rem(id_user)
            
            
def lab_day_key(bot, update):
    lab = update.message.text[5]
    sel = unibotpy.SELEZIONA_GIORNO
    key = unibotpy.lab_day_markup(lab)
    bot.sendMessage(chat_id=update.message.chat_id, text=sel, reply_markup=ReplyKeyboardMarkup(key, resize_keyboard=True, one_time_keyboard=True))
    

#===============================================================================
# Invia un messaggio con la lista delle aule
#===============================================================================
def aule(bot, update):
    reply = unibotpy.aule()
    bot.sendMessage(chat_id=update.message.chat_id, text=reply, parse_mode=ParseMode.HTML, reply_markup=ReplyKeyboardHide(hide_keyboard=True))
def orario_aula (bot, update):
    aula = update.message.text[6:]
    grid = unibotpy.orario_aula(aula)
    bot.sendMessage(chat_id=update.message.chat_id, text=grid, parse_mode=ParseMode.MARKDOWN)
def orario_materia(bot, update):
    key = update.message.text[1:]  # elimino /
    grid = unibotpy.orario_materia(key)
    bot.sendMessage(chat_id=update.message.chat_id, text=grid, parse_mode=ParseMode.HTML)
def search (bot, update):
    term = update.message.text[7:]
    if (term == ""):  # se non e' stato inserito nessun testo
        id_user = update.message.from_user["id"]
        db.set(id_user, 'need_search')  # salvo sul db l'id dell'utente con lo stato che indica che e' in attesa del testo
        bot.sendMessage(chat_id=update.message.chat_id, text="Digita il nome di un insegnamento o di un docente", reply_markup=ForceReply(force_reply=True))
    else: 
        inf, keyList = unibotpy.search(term, False)
        for key in keyList:
                dp.add_handler(CommandHandler(key, orario_materia))  # crea gli handler per ogni prenotazione restituita
        bot.sendMessage(chat_id=update.message.chat_id, text=inf, parse_mode=ParseMode.HTML, reply_markup=ReplyKeyboardHide(hide_keyboard=True))


def inlinequery(bot, update):
    query = update.inline_query.query
    results = list()

    results.append(InlineQueryResultArticle(id=uuid4(),
                                            title="Cerca una materia o un docente",
                                            input_message_content=InputTextMessageContent(
                                                unibotpy.search(query, True))))

    bot.answerInlineQuery(update.inline_query.id, results=results, cache_time=84500)


def error(bot, update, error):
    try:
        ch_id = "4901699"
        starter = Bot(token=token)
        txt = "Errore bot"
        starter.sendMessage(ch_id, text=txt)
    except:
        pass
    logging.warn('Update "%s" caused error "%s"' % (update, error))


def main():
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)
    start_handler = CommandHandler("start", start)
    aule_handler = CommandHandler('aule', aule)
    lab_handler = CommandHandler('lab', lab)
    search_handler = CommandHandler('cerca', search)
    freelab_handler = CommandHandler('labfree', lab_free)
    info_handler = CommandHandler('info', start)

    dp.add_handler(start_handler)
    dp.add_handler(info_handler)
    dp.add_handler(aule_handler)
    dp.add_handler(lab_handler)
    dp.add_handler(search_handler)
    dp.add_handler(freelab_handler)
    dp.add_handler(InlineQueryHandler(inlinequery))
    for command in unibotpy.command_aule():
        dp.add_handler(CommandHandler(command, orario_aula))
    # comandi lab
    for i in range(1, 7):
        dp.add_handler(CommandHandler('lab_' + str(i), lab_day_key))
    dp.add_handler(MessageHandler([Filters.text] , input_parser))

    #dp.addErrorHandler(error)
    updater.start_polling()
    
    ch_id = '' # chat id personale
    starter = Bot(token=token)
 
    txt = "Sono attivo"
    starter.sendMessage(ch_id, text=txt)
    
    updater.idle()
    
    txt = "Bot fermo!"
    starter.sendMessage(ch_id, text=txt)
    
if __name__ == '__main__':
    main()

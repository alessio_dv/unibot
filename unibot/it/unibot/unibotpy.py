import requests
import ConfigParser
import urllib
import datetime
from telegram.emoji import Emoji
from tabulate import tabulate
import time
from collections import OrderedDict
import json

HEADERS = {'content-type': ''}

config = ConfigParser.ConfigParser()
config.read('url.ini')
URL = str(config.get('main', 'url'))
URL_MOBILE = str(config.get('main', 'url_mobile'))
URL_LAB = str(config.get('main', 'url_lab'))
SELEZIONA_GIORNO = "Seleziona il giorno"
CALENDAR_WEEK = ["LUN", "MAR", "MER", "GIO", "VEN"]
ORARI = ['08-09', '09-10', '10-11', '11-12', '12-13', '13-14', '14-15', '15-16', '16-17', '17-18', '18-19']

#===============================================================================
# A partire dalla prima domenica calcolo tutti i giorni e le settimane dell'anno corrente
# Funzione ausiliaria --> []
#===============================================================================
def allSundays():
    year = datetime.date.today().year
    """This code was provided in the previous answer! It's not mine!"""
    d = datetime.date(year, 1, 1)  # January 1st                                                          
    d += datetime.timedelta(days=6 - d.weekday())  # Prima domenica                                                         
    while d.year == year:
        yield d
        d += datetime.timedelta(days=7)



#===============================================================================
# Data la settimana corrente restituisce il timestamp del primo giorno della settimana (Dom) e l'ultimo int --> timestamp
# Funzione ausiliaria int -> timestamp, timestamp
#===============================================================================
def currentWeek(week):
    CALENDAR_YEAR = allSundays()  # calcolo le settimane evitando di farlo ad ogni richiesta
    Dict = {}
    for wn, d in enumerate(CALENDAR_YEAR):
        Dict[wn + 1] = [(d + datetime.timedelta(days=k)) for k in range(0, 7) ]
    currentWeek = Dict[week]
    time_start = int(time.mktime(currentWeek[0].timetuple()))
    time_end = int(time.mktime(currentWeek[6].timetuple()))
    return str(time_start), str(time_end)


#===============================================================================
# Crea la tastiera dei giorni per i lab #microscope
#===============================================================================


def lab_day_markup(lab):
    day_markup = [[Emoji.MICROSCOPE + "[" + str(lab) + "] " + CALENDAR_WEEK[i] for i in range(0, 5)]]
    day_markup.append(['/lab','/start'])
    return day_markup


def labfree_day_markup():
    day_markup = [[u'\U0001f193' + u'\U0001F52C' + " " + CALENDAR_WEEK[i] for i in range(0, 5)]]
    day_markup.append(['/start'])
    return day_markup


#===============================================================================
# Crea la tastiera dei giorni per i lab #book
#===============================================================================
def aula_day_markup(lab):
    day_markup = [[Emoji.BOOKS + "[" + str(lab) + "] " + CALENDAR_WEEK[i] for i in range(0, 5)]]
    day_markup.append(['/aule'])
    return day_markup


def start_markup():
    start_markup = [['/aule', '/lab', '/cerca'], ['/labfree', '/info']]
    return start_markup


#===============================================================================
# restituisce un nuovo dizionario predefinito
# --> {}
#===============================================================================
def makeNewGridCalendar():
    grid_calendar = OrderedDict([('', ['08-09', '09-10', '10-11', '11-12', '12-13', '13-14', '14-15', '15-16', '16-17', '17-18', '18-19']), ('LUN', ['', '', '', '', '', '', '', '', '', '', '']), \
                              ('MAR', ['', '', '', '', '', '', '', '', '', '', '']), ('MER', ['', '', '', '', '', '', '', '', '', '', '']), \
                              ('GIO', ['', '', '', '', '', '', '', '', '', '', '']), ('VEN', ['', '', '', '', '', '', '', '', '', '', ''])])
    return grid_calendar


#===============================================================================
# timestamp -> datetime
#===============================================================================
def tsTOdt(ts):
    return datetime.datetime.fromtimestamp(
       int(ts))


#===============================================================================
# Pulizia e formattazione delle descrizioni delle prenotazioni nei lab
#===============================================================================
def saniTitle(title, id_pren):
    title = title.replace("<br />", " ")
    title = title.replace("<br/>", "")
    title = title.replace("53", "")
    if (title == "Altro "):
        return title
    elif ("Altro" in title):
        title = title.replace("Altro", "")
        title = title[:8]
        return title + ".." + id_pren
    else:
        title = title[:8]
        return title + ".." + id_pren
    

#===============================================================================
# Split di un dizionario per questioni grafiche
#===============================================================================
def splitDict(d):
    d1 = OrderedDict(d.items()[:2])
    d2 = OrderedDict(d.items()[2:4])
    d3 = OrderedDict(d.items()[4:])
    return d1, d2, d3


#===============================================================================
# Dato il dizionario e il giorno crea un nuovo dizionario che contiene solo quel giorno
#===============================================================================
def splitDict_day(d, day):
    grid_day = {k: d[k] for k in ('', day)}
    return grid_day


#===============================================================================
# @StackOverflow
# Funzione ausiliare per aggiungere un elemento in testa ad un Ordereddict
#===============================================================================
def ordered_dict_prepend(dct, key, value, dict_setitem=dict.__setitem__):
    root = dct._OrderedDict__root
    first = root[1]

    if key in dct:
        link = dct._OrderedDict__map[key]
        link_prev, link_next, _ = link
        link_prev[1] = link_next
        link_next[0] = link_prev
        link[0] = root
        link[1] = first
        root[1] = first[0] = link
    else:
        root[1] = first[0] = dct._OrderedDict__map[key] = [root, first, key]
        dict_setitem(dct, key, value)
        


#===============================================================================
# Restituisce la lista delle aule in formato per i comandi
#===============================================================================
def command_aule():
    mList = []
    for key in requests.get(URL_MOBILE + 'GetAule/', headers=HEADERS).json():  # richiesta web
        mList.append(key["Aula"].encode('utf-8').replace (" ", "_").lower())
    return mList


#===============================================================================
# Restituisce la lista delle aule
#===============================================================================
def aule():
    mList = []
    reply = u'\U0001f3db' + "<b>Aule</b>" u'\U0001f3db' + "\n"
    for key in requests.get(URL_MOBILE + 'GetAule/', headers=HEADERS).json():  # richiesta web
        mList.append(key["Aula"].encode('utf-8').replace (" ", "_").lower())
    i = 0
    for aula in mList:
        if (i == 2):
            reply += '\n<pre>=================</pre>\n'
            i = 0
        reply += '/' + aula + " "
        i += 1
    return reply


#===============================================================================
# Data un'aula restituisce l'orario
#===============================================================================
def orario_aula(aula):
    now = datetime.datetime.now();
    today = now.strftime("%m/%d/%Y")  # data attuale
    urlToday = urllib.quote(today, safe='')  # data econdata (mm%2Fdd%2FYY)
    r = requests.get(URL_MOBILE + 'GetOrarioAula?codAula=' + aula + '&dataOrario=' + urlToday)  # il sito restituisce testo e non json
    mlist = json.loads(r.text)  # converto la stringa in json
    if all(o is None for o in mlist["TabellaOrari"]):
        return (u'\U0001f3db' + "*Aula " + aula + "*\n" + u'\U0001f4da' + "Nessuna lezione in questa settimana oppure orario non disponibile!")
    #===========================================================================
    # Parte da testare in mancanza di orari
    #===========================================================================
    grid = mlist["GrigliaOrari"]  # il sito restituisce gia un campo con la griglia degli orari in una lista di array.
    grid = [ORARI] + grid  # aggiungo gli orari al dizionario
    grid_dict = OrderedDict(zip([''] + CALENDAR_WEEK, grid))  # faccio lo zip tra l'array dei giorni (chiavi del dizionario) e gli array restituiti in json (valori)


#===============================================================================
# Restituisce gli insegnamenti con i docenti e gli orari (no orari in caso di inline)
# string, Optional[Bool] --> string, []
#===============================================================================
def search(term, inline):
    mlist = []
    keyList = []
    inf = ""
    ciclo = ['1', '2']
    if term == "": #Necessario per il comando inline
        return ""
    for c in ciclo:
        for key in requests.get(URL + 'CorsiDiStudio/Search?term=' + term + '&ciclo=' + c, headers=HEADERS).json():
            key.update({'ciclo': c})  # aggiungo il ciclo
            mlist.append(key)
    if (inline): #Funzioni inline
        if (mlist == []):
            return "Nessun insegnamento o docente trovato!"
        for s in mlist:
            if (s["Docenti"] == []):
                inf += u'\U0001f4da' + s["Description"] + " [" + s["ciclo"] + " ciclo] - " + s["CorsoDiStudio"] + "\n===================\n"
            else:
                docenti = ', '.join(s["Docenti"])
                inf += u'\U0001f4da' + s["Description"] + " [" + s["ciclo"] + " ciclo] - " + s["CorsoDiStudio"] + "\n "\
                + u'\U0001f464' + docenti + "\n===================\n"
        return inf
    if (mlist == []):
        return "Nessun insegnamento o docente trovato!", []
    for s in mlist:
        keyList.append(str(s["CvgKey"]))
        if (s["Docenti"] == []):
            inf += u'\U0001f4da' + s["Description"] + " [" + s["ciclo"] + " ciclo] - " + s["CorsoDiStudio"] + \
            u'\u23F0' + "Orario: /" + str(s["CvgKey"]) + "\n<pre>===================</pre>\n"
        else:
            docenti = ', '.join(s["Docenti"])
            inf += u'\U0001f4da' + s["Description"] + " [" + s["ciclo"] + " ciclo] - " + s["CorsoDiStudio"] + "\n "\
            u'\u23F0' + "Orario: /" + str(s["CvgKey"]) + "\n" + u'\U0001f464' + docenti + "\n<pre>===================</pre>\n"
    return inf, keyList


#===============================================================================
# Restituisce l'orario di una materia dato il codice (utilizzabile dopo la ricerca)
#===============================================================================
def orario_materia(cod):
    mlist = []
    inf = ""
    ciclo = ['1', '2']
    for c in ciclo:
        for key in requests.get(URL + 'CorsiDiStudio/Load?keys=' + cod + '&ciclo=' + c, headers=HEADERS).json():
            key.update({'ciclo': c})  # aggiungo il ciclo
            mlist.append(key)
    for o in mlist:
        if (o["Orario"] == []):
            inf = u'\U0001f4da' + o["Description"] + " [" + o["ciclo"] + " ciclo] - " + o["CorsoDiStudio"] + "\n<b>Orario non disponbile</b>"
            return inf
        else:
            return "test"
    #===========================================================================
    # Parte da testare quando saranno disponibili gli orari
    #===========================================================================
    #===========================================================================
    # grid_calendar = makeNewGridCalendar()
    # ts_start = 2
    # ts_end = 5
    # for h in range(ts_start,ts_end):
    #     grid_calendar[CALENDAR_WEEK[3]][h] = 'aula' #marco con il nome dell'aula tutti gli orari nei giorni dove c'e lezione
    #     grid1, grid2 = splitDict(grid_calendar)
    #     calendar_day1 = tabulate(grid1, headers="keys", tablefmt="orgtbl", stralign="center")  # metto in tabulazione
    #     calendar_day2= tabulate(grid2, headers="keys", tablefmt="orgtbl", stralign="center")
    # return "<pre>" + calendar_day + "</pre>"
    #===========================================================================
    

#===============================================================================
# Crea la griglia degli orari popolando il dizionario
#===============================================================================
def griglia_orari(oList, day):
    if (day == 'all_grid'):
        if (oList == []):
            return {}
    elif (oList == []):
        return "Laboratorio libero per tutto il giorno", []
    prenList = []
    grid_calendar = makeNewGridCalendar()
    for o in oList:  # riempio la griglia con le prenotazioni dell'intera settimana
        id_pren = " /" + str(o["id"])
        title = saniTitle(o["title"], id_pren)
        date_start = tsTOdt(str(o["start"])[:-3])
        date_end = tsTOdt(str(o["end"])[:-3])
        hour_start = date_start.hour - 8
        hour_end = date_end.hour - 8
        for h in range(hour_start, hour_end):  # riempio tutte le caselle nel range di orario della prenotazione
            grid_calendar[CALENDAR_WEEK[date_start.weekday()]][h] = title
            # grid_calendar[CALENDAR_WEEK[date_start.weekday()]][hour_end] = saniTitle(o["title"]) + " /" + str(o["id"])+""
        prenList.append(str(o["id"]))
    if (day == 'all_grid'):
        return grid_calendar
    elif day == '':
        grid1, grid2 = splitDict(grid_calendar)
        calendar1 = tabulate(grid1, headers="keys", tablefmt="simple")
        calendar2 = tabulate(grid2, headers="keys", tablefmt="simple")
        return calendar1, calendar2
    else:  # giorno specifico
        grid_day = splitDict_day(grid_calendar, day)  # Seleziono solo il giorno che mi interessa
        calendar_day = tabulate(grid_day, headers="keys", tablefmt="simple")  # metto in tabulazione
        return calendar_day, prenList
      
    
#===============================================================================
# Restituisce dato un lab e un giorno (opzionale) la griglia orari
#===============================================================================
def orario_lab(lab, day):
    #   ts_start = str(time.time()).split('.')[0]  # timestamp 604800
    #  ts_end = str(int(ts_start) + 86400)
    week = datetime.datetime.now().isocalendar()[1]
    ts_start, ts_end = currentWeek(week)
    mlist = []
    # print URL_LAB + 'get_prenotazione.php?id_aula=' + lab  + '&start=' + ts_start + '&end=' + ts_end + '&device=mobile'
    for key in requests.get(URL_LAB + 'get_prenotazione.php?id_aula=' + lab + '&start=' + ts_start + '&end=' + ts_end + '&device=mobile', headers=HEADERS).json():
        mlist.append(key)
    if day == '':
        return griglia_orari(mlist, '')
    else:
        return griglia_orari(mlist, day)
    
#===============================================================================
# Restituisce il dettaglio di una prenotazione (utilizzabile dal calendario giornaliero di un lab)
#===============================================================================
def dett_prenotazione_lab(pren):
    r = requests.get(URL_LAB + 'get_prenotazione.php?id_prenotazione=' + pren + '&device=mobile')  # il sito restituisce testo e non json
    mlist = json.loads(r.text)  # converto la stringa in json
    date_start = tsTOdt(str(mlist["start"]))
    date_end = tsTOdt(str(mlist["end"]))
    print URL_LAB + 'get_prenotazione.php?id_prenotazione=' + pren + '&device=mobile'
    if (mlist["docins"].find('Empty') == 0):
        descr = mlist["notes"]
    else:
        descr = mlist["docins"]
    # formatto la risposta
    dettaglio = u"\U0001F52C" + "<b>LAB " + mlist['id_aula'] + "</b>" + u"\U0001F52C" \
                + "<pre>==========</pre>\n" + u"\u23F0" + str(date_start.hour) + "-" + str(date_end.hour) + u"\u23F0" \
                + ": \n" + descr
    return dettaglio


#===============================================================================
# Restituisce la lista dei laboratiori
#===============================================================================
def lab():
    lab = u"\U0001F52C" + "LABORATORI" + u"\U0001F52C" + "\n"
    for i in range(1, 6):
        if (i == 4):
            lab += "\n<pre>================</pre> \n"
        lab += "/lab_" + str(i) + " "
    return lab


#===============================================================================
# Ricerca in quel giorno la disponibilita dei laboratori
#===============================================================================
def lab_free(day):
    allRequest = []
    allGrid = OrderedDict()
    week = datetime.datetime.now().isocalendar()[1]
    ts_start, ts_end = currentWeek(week)
    grid_day = OrderedDict()
    for lab in range(1,6):
        mList = []
        for key in requests.get(URL_LAB + 'get_prenotazione.php?id_aula=' + str(lab) + '&start=' + ts_start + '&end=' + ts_end + '&device=mobile', headers=HEADERS).json():
            mList.append(key)
        allRequest.append(mList)
    for index,r in enumerate(allRequest):
        if griglia_orari(r, 'all_grid') != {}:
            dictDay = splitDict_day(griglia_orari(r, 'all_grid'), day) #Seleziono il giorno da ogni dizionario
        else:
            dictDay = {}
        allGrid.update({"LAB" + str(index+1) : dictDay}) #griglia delle griglie orari (dict(dict))
    list_lab = ['LAB1','LAB2','LAB3','LAB4','LAB5']
    for k in list_lab:
        grid_day.update({k: invertCheckFree(allGrid[k],day)})
    d1,d2, d3 = splitDict(grid_day)
    ordered_dict_prepend(d1, str(day), ORARI)
    ordered_dict_prepend(d2, str(day), ORARI)
    ordered_dict_prepend(d3, str(day), ORARI)
    calendar1 = tabulate(d1, headers="keys", tablefmt="orgtbl", stralign='center')
    calendar2 = tabulate(d2, headers="keys", tablefmt="orgtbl",stralign='center')
    calendar3 = tabulate(d3, headers="keys", tablefmt="orgtbl",stralign='center')
    return calendar1, calendar2, calendar3


#===============================================================================
# Funzione ausiliaria di lab_free. Gli orari occupati vengono contrassegnati con '' quelli vuoti con X
#===============================================================================
def invertCheckFree(k,day):
    if k == {}:
        listHour = ["X" for _ in range(0,11)]
        return listHour
    listHour = k[day]
    for i,o in enumerate(listHour):
        if (o != ""):
            listHour[i] = ""
        else:
            listHour[i] = "X"
    return listHour


#===============================================================================
#  test
#===============================================================================
# orario_aula("aula_01")
# orario_lab('2')
# griglia_orari([])
# print currentWeek(40) ---> 
# search('corradi')
#lab_free("LUN")
#invertCheckFree({'' : [],'LUN' : ['prova', '', 'er', '']}, 'LUN')

import sqlite3
con = sqlite3.connect('allstate.db')

with con:   
    cur = con.cursor()        
    cur.execute("DROP TABLE IF EXISTS states")
    cur.execute('''CREATE TABLE states (u_id text, state text)''')